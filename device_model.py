#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import settings
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import csv
import sys
import utils
from pprint import pprint

#HTTP API Call Example
# http://demo.piwik.org/?module=API&method=UserCountry.getCountry&idSite=all&date=yesterday&period=day&format=xml&filter_truncate=5&language=en&segment=entryPageUrl==http%3A%2F%2Fwww.virtual-drums.com%2F

# Piwik http API call format
# baseurl + module + method + idSite + period + format + api_token

# Base URL
# Change it to suit your own URL
BASE_URL = settings.BASE_URL

# module and methods
MODULE = "?module=API"
METHOD_DEVICES_BRAND = "&method=DevicesDetection.getBrand"
METHOD_DEVICES_MODEL = "&method=DevicesDetection.getModel"

# ID of Site
ID_SITES_ALL = "&idSite=all" # get all sites
ID_SITE_NAME = "&idSite="

# Expanded
EXPANDED = "&expanded=1"

# Data Format output
FORMAT_JSON = "&format=json"
FORMAT_CSV = "&format=csv"

ARG_PERIOD=sys.argv[1]

file_name = "devices_model-%s.csv" % (ARG_PERIOD.replace(",", "_"))

# Period
PERIOD = "&period=range&date=" + ARG_PERIOD

# this token is used to authenticate your API request.
# You can get the token on the API page inside your Piwik interface i.e.
# By logging in Piwik, then click on your username in the top menu, then click the link “API” in the left menu.
TOKEN_AUTH = "&token_auth=" + settings.TOKEN_AUTH

# dictionary
device_metrics = []

# fn for calling page titles
def get_device_info(base_url,module,method,site_id,output_format,period,expanded,api_token):
    API_PAGE_URL = base_url + module + method + site_id + output_format + period + expanded + api_token
    print API_PAGE_URL

    call_device_model = requests.get(API_PAGE_URL, verify=False)

    sites = call_device_model.json()

    data_file = csv.writer(open(file_name,"w"))
    data_file.writerow(['Site ID',
                         'Site Name',
                         'Device Model',
                         'Device Model Unique Visits'])

    for site_id in sites:
        site = sites[site_id]
        site_name = utils.siteID2name(site_id)
        for model in site:
            # device_metrics.append({'site_id':site_id,'site_name':site_name,'brand':site[brand][u'label'],'unique_visitors':site[brand][u'sum_nb_unique_visitors']})
            data_file.writerow([site_id, site_name, str(model['label']), model[u'sum_daily_nb_uniq_visitors']])

    print "Data written out to %s" % file_name


get_device_info(BASE_URL, MODULE, METHOD_DEVICES_MODEL, ID_SITES_ALL, FORMAT_JSON,PERIOD,EXPANDED,TOKEN_AUTH)
