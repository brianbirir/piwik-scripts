from datetime import datetime
import requests
import settings


def parseDate(myDate):
    return datetime.strptime(myDate, "%Y-%m-%d")


def siteID2name(site_id):
    url = "%s/?module=API&method=SitesManager.getSiteFromId&idSite=%s&format=json&token_auth=%s" % (settings.BASE_URL, site_id, settings.TOKEN_AUTH)
    site = requests.get(url, verify=False)
    return str(site.json()[0][u'name'])

filesizes = {}


def findDownloadSize(address):
    if address in filesizes:
        print "[INFO]: Getting cached filesize"
    else:
        r = requests.head(address)
        filesizes[address] = r.headers['content-length']

    return round(float(filesizes[address]) / 1024 / 1024, 2)
