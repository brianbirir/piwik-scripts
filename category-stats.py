#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import settings
import requests
import csv
from pprint import pprint

# Script for getting time spent, number of visits and page views per category
# Categories are links found on the landing page

#HTTP API Call Example
# http://demo.piwik.org/?module=API&method=UserCountry.getCountry&idSite=all&date=yesterday&period=day&format=xml&filter_truncate=5&language=en&segment=entryPageUrl==http%3A%2F%2Fwww.virtual-drums.com%2F

# Piwik http API call format
# baseurl + module + method + idSite + period + format

# Base URL
# Change it to suit your own URL
BASE_URL = settings.BASE_URL

# module and methods
MODULE = "?module=API"
METHOD_PAGES = "&method=Actions.getPageUrls"
METHOD_SITE_NAME_BY_ID = "&method=SitesManager.getSiteFromId"

# ID of Site
ID_SITE = "&idSite=all" # get all sites
ID_SITE_VAR = "&idSite="

# Data Format output
FORMAT_JSON = "&format=json"
FORMAT_CSV = "&format=csv"

ARG_PERIOD=sys.argv[1]

file_name = "category_stats-%s.csv" % (ARG_PERIOD.replace(",", "_"))

# Period
PERIOD = "&period=range&date=" + ARG_PERIOD

# this token is used to authenticate your API request.
# You can get the token on the API page inside your Piwik interface i.e.
# By logging in Piwik, then click on your username in the top menu, then click the link “API” in the left menu.
TOKEN_AUTH = "&token_auth=" + settings.TOKEN_AUTH

# dictionary
page_metrics = []

# fn for calling page titles
def get_page_title(base_url,module,method,site_id,output_format,period,api_token):
    API_PAGE_URL = base_url + module + method + site_id + output_format + period + api_token

    call_page_title = requests.get(API_PAGE_URL, verify=False)

    pages_json = call_page_title.json()

    for site in pages_json:
        if pages_json[site] == None:
            continue

        API_URL_SITE_NAME = BASE_URL + MODULE + METHOD_SITE_NAME_BY_ID + ID_SITE_VAR + site + FORMAT_JSON + TOKEN_AUTH
        call_site_name = requests.get(API_URL_SITE_NAME,verify=False)
        sites = call_site_name.json()


        for site_property in sites:
            site_name = site_property['name']

            for metrics in pages_json[site]:
                no_visits = metrics['nb_visits']
                total_time_spent = metrics['sum_time_spent']
                page_views = metrics['nb_hits']
                page_label = metrics['label']

                # get site name
                page_metrics.append({"site_id":site,"site_name":site_name,"page":page_label,"time":total_time_spent,"visits":no_visits,"views":page_views})

        data_file = csv.writer(open(file_name,"wb"))
        data_file.writerow(['Site ID','Site Name','Page','Time Spent','No of Visits','Page Views'])

        for metric in page_metrics:
            data_file.writerow([metric["site_id"],
                                metric["site_name"].encode("utf-8"),
                                metric["page"].encode("utf-8"),
                                metric["time"],
                                metric["visits"],
                                metric["views"]])


get_page_title(BASE_URL, MODULE, METHOD_PAGES, ID_SITE, FORMAT_JSON, PERIOD, TOKEN_AUTH)
