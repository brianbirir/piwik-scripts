#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import settings
import requests
import csv
from pprint import pprint

# Script for getting number page views per page title per site
# HTTP API Call Example
# http://demo.piwik.org/?module=API&method=UserCountry.getCountry&idSite=all \
# &date=yesterday&period=day&format=xml&filter_truncate=5&language=en \
# &segment=entryPageUrl==http%3A%2F%2Fwww.virtual-drums.com%2F

# Piwik http API call format
# baseurl + module + method + idSite + period + format + api_token

# Base URL
# Change it to suit your own URL
BASE_URL = settings.BASE_URL

# module and methods
MODULE = "?module=API"
METHOD_PAGE_TITLES = "&method=Actions.getPageTitles"
METHOD_SITE_NAME_BY_ID = "&method=SitesManager.getSiteFromId"

# ID of Site
ID_SITE = "&idSite=all" # get all sites
ID_SITE_VAR = "&idSite="

# Data Format output
FORMAT_JSON = "&format=json"
FORMAT_CSV = "&format=csv"

ARG_PERIOD=sys.argv[1]

file_name = "no_of_page_views-%s.csv" % (ARG_PERIOD.replace(",", "_"))

# Period
PERIOD = "&period=range&date=" + ARG_PERIOD

# this token is used to authenticate your API request.
# You can get the token on the API page inside your Piwik interface i.e.
# By logging in Piwik, then click on your username in the top menu, then click
# the link “API” in the left menu.
TOKEN_AUTH = "&token_auth=" + settings.TOKEN_AUTH

# dictionary
page_metrics = []

# fn for calling page titles
def get_page_views(base_url,module,method,site_id,output_format,period,api_token):
    API_PAGE_URL = base_url + module + method + site_id + output_format + period + api_token

    call_page_title = requests.get(API_PAGE_URL,verify=False)

    pages_json = call_page_title.json()

    for site in pages_json:
        if pages_json[site] == None:
            continue

        API_URL_SITE_NAME = BASE_URL + MODULE + METHOD_SITE_NAME_BY_ID + ID_SITE_VAR + site + FORMAT_JSON + TOKEN_AUTH
        call_site_name = requests.get(API_URL_SITE_NAME,verify=False)
        sites = call_site_name.json()


        for site_property in sites:
            site_name = site_property['name']

            for metrics in pages_json[site]:
                number_page_views = metrics['nb_hits']
                page_label = metrics['label']


                page_metrics.append({"site_id":site,"site_name":site_name,"page_title":page_label,"page_hits":number_page_views})

        data_file = csv.writer(open(file_name,"wb"))
        data_file.writerow(['Site ID','Site Name','Page Title','No of Page Views'])

        for metric in page_metrics:
            data_file.writerow([metric["site_id"],
                                metric["site_name"],
                                metric["page_title"],
                                metric["page_hits"]])

    print "Data written out to %s" % file_name

get_page_views(BASE_URL, MODULE, METHOD_PAGE_TITLES, ID_SITE, FORMAT_JSON, PERIOD, TOKEN_AUTH)
