#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import settings
import requests
import csv
import utils
from pprint import pprint

# HTTP API Call Example
# http://demo.piwik.org/?module=API&method=UserCountry.getCountry&idSite=all&date=yesterday&period=day&format=xml&filter_truncate=5&language=en&segment=entryPageUrl==http%3A%2F%2Fwww.virtual-drums.com%2F

# Piwik http API call format
# baseurl + module + method + idSite + period + format + api_token

# Base URL
# Change it to suit your own URL
BASE_URL = settings.BASE_URL

# module and methods
MODULE = "?module=API"
METHOD_DOWNLOADS = "&method=Actions.getDownloads"
METHOD_SITE_NAME_BY_ID = "&method=SitesManager.getSiteFromId"

# ID of Site
ID_SITES_ALL = "&idSite=all"  # get all sites
ID_SITE_NAME = "&idSite="

# Expanded
EXPANDED = "&expanded=1"

# Data Format output
FORMAT_JSON = "&format=json"
FORMAT_CSV = "&format=csv"

ARG_PERIOD = sys.argv[1]

file_name = "downloaded_content-%s.csv" % (ARG_PERIOD.replace(",", "_"))

# Period
PERIOD = "&period=range&date=" + ARG_PERIOD

# this token is used to authenticate your API request.
# You can get the token on the API page inside your Piwik interface i.e.
# By logging in Piwik, then click on your username in the top menu, then click the link “API” in the left menu.
TOKEN_AUTH = "&token_auth=" + settings.TOKEN_AUTH


# dictionary
download_metrics = []


# fn for calling page titles
def get_downloads(base_url, module, method, site_id, output_format, period, expanded, api_token):
    API_PAGE_URL = base_url + module + method + site_id + \
        output_format + period + expanded + api_token

    print "[INFO]: %" + API_PAGE_URL
    call_downloads = requests.get(API_PAGE_URL, verify=False)

    pages_downloads = call_downloads.json()

    # get site id for specific site
    for site_id in pages_downloads:
        if not pages_downloads[site_id]:
            continue

        API_URL_SITE_NAME = BASE_URL + MODULE + METHOD_SITE_NAME_BY_ID + \
            ID_SITE_NAME + site_id + FORMAT_JSON + "&token_auth=" + TOKEN_AUTH
        call_site_name = requests.get(API_URL_SITE_NAME, verify=False)
        sites = call_site_name.json()

        # get name of specific site
        for site_property in sites:
            site_name = site_property['name']
            # get site property called sub table
            for download_property in pages_downloads[site_id]:
                # get data from subtable
                for downloads in download_property['subtable']:
                    downloads_label = downloads['label']
                    downloads_hits = downloads['nb_hits']
                    downloads_visits = downloads['nb_visits']
                    downloads_unique_visits = downloads[
                        'sum_daily_nb_uniq_visitors']
                    downloads_url = downloads['url']

                    # append downloads metrics
                    download_metrics.append({"site_id": site_id,
                                             "site_name": site_name,
                                             "download_label": downloads_label,
                                             "download_hits": downloads_hits,
                                             "downloads_visits": downloads_visits,
                                             "downloads_unique_visits": downloads_unique_visits,
                                             "downloads_url": downloads_url})
        data_file = csv.writer(open(file_name, "w"))
        data_file.writerow(['Site ID',
                            'Site Name',
                            'Download Name',
                            'Download Size (MegaBytes)',
                            'Download Hits',
                            'Total Download Size (size x hits)',
                            'Download Visits', 'Uniques Visits',
                            'Download URL'])

        total_downloads = 0
        for metric in download_metrics:
            url = settings.MEDIAL_LOCATION + metric["download_label"]
            download_size = utils.findDownloadSize(url)
            total_downloads += download_size * metric["download_hits"]
            data_file.writerow([metric["site_id"],
                                metric["site_name"],
                                metric["download_label"],
                                download_size,
                                metric["download_hits"],
                                download_size * int(metric["download_hits"]),
                                metric["downloads_visits"],
                                metric["downloads_unique_visits"],
                                metric["downloads_url"]])

        data_file.writerow(["Total download size", "", "", "", "",
                            total_downloads])

    print "Data written out to %s" % file_name

get_downloads(BASE_URL, MODULE, METHOD_DOWNLOADS, ID_SITES_ALL,
              FORMAT_JSON, PERIOD, EXPANDED, TOKEN_AUTH)
