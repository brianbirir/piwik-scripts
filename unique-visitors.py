#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import settings
import requests
import csv
from pprint import pprint

# This script gets the number of unique users per site per month

#HTTP API Call Example
# http://demo.piwik.org/?module=API&method=UserCountry.getCountry&idSite=all&date=yesterday&period=day&format=xml&filter_truncate=5&language=en&segment=entryPageUrl==http%3A%2F%2Fwww.virtual-drums.com%2F

# Piwik http API call format
# baseurl + module + method + idSite + period + format + api_token

# Base URL
# Change it to suit your own URL
BASE_URL = settings.BASE_URL

# module and methods
MODULE = "?module=API"
METHOD_SITE_NAME_BY_ID = "&method=SitesManager.getSiteFromId"
METHOD_UNIQUE_VISITORS = "&method=VisitsSummary.getUniqueVisitors"

# ID of Site
ID_SITE = "&idSite=all"
ID_SITE_VAR = "&idSite="

# Data Format output
FORMAT_JSON = "&format=json"
FORMAT_CSV = "&format=csv"

ARG_PERIOD=sys.argv[1]

file_name = "unique_visitors-%s.csv" % (ARG_PERIOD.replace(",", "_"))

# Period
PERIOD = "&period=range&date=" + ARG_PERIOD

# this token is used to authenticate your API request.
# You can get the token on the API page inside your Piwik interface i.e.
# By logging in Piwik, then click on your username in the top menu, then click
# the link “API” in the left menu.
TOKEN_AUTH = "&token_auth=" + settings.TOKEN_AUTH

unique_visitors_metrics = []

# fn for calling page titles
def get_unique_visitor(base_url,module,method,site_id,output_format,period,api_token):
    API_SITES = base_url + module + method + site_id + output_format + period + api_token
    print "[INFO]: %s" % API_SITES
    called_sites = requests.get(API_SITES,verify=False)
    sites_json = called_sites.json()
    if 'message' in sites_json:
        print "[FATAL]:  %s. Message: %s" % ("PIWIK not configured properly",
                                            sites_json['message'])
        sys.exit(1)

    # get site id and get the json data from each site
    for site_id in sites_json:
        unique_visitors = sites_json[site_id]

        API_URL_SITE_NAME = BASE_URL + MODULE + METHOD_SITE_NAME_BY_ID + ID_SITE_VAR + site_id + FORMAT_JSON + TOKEN_AUTH
        call_site_name = requests.get(API_URL_SITE_NAME,verify=False)
        print "[INFO]: %s" % API_URL_SITE_NAME
        sites_name = call_site_name.json()

        # get site name
        for site_property in sites_name:
            site_name = site_property['name']

        unique_visitors_metrics.append({'site_id': site_id,
                                        'site_name': site_name,
                                        'unique_visitors': unique_visitors})

    # write data to csv file
    data_file = csv.writer(open(file_name, "wb"))
    data_file.writerow(['Site ID', 'Site Name', 'Number of Unique Visitors'])

    for uvm in unique_visitors_metrics:
        data_file.writerow([uvm['site_id'],
                            uvm['site_name'],
                            uvm['unique_visitors']])

    print "Data written out to %s" % file_name

get_unique_visitor(BASE_URL, MODULE, METHOD_UNIQUE_VISITORS,ID_SITE, FORMAT_JSON, PERIOD, TOKEN_AUTH)
