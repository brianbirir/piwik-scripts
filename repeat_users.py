#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import settings
import requests
import csv
import sys
import utils
from pprint import pprint

#HTTP API Call Example
# http://demo.piwik.org/?module=API&method=UserCountry.getCountry&idSite=all&date=yesterday&period=day&format=xml&filter_truncate=5&language=en&segment=entryPageUrl==http%3A%2F%2Fwww.virtual-drums.com%2F

# Piwik http API call format
# baseurl + module + method + idSite + period + format + api_token

# Base URL
# Change it to suit your own URL
BASE_URL = settings.BASE_URL

# module and methods
MODULE = "?module=API"
METHOD_VISITS_SUMMARY = "&method=VisitsSummary.get"
METHOD_SITE_NAME_BY_ID = "&method=SitesManager.getSiteFromId"

# ID of Site
ID_SITES_ALL = "&idSite=all" # get all sites
ID_SITE_NAME = "&idSite="

# Expanded
EXPANDED = "&expanded=1"

# Data Format output
FORMAT_JSON = "&format=json"
FORMAT_CSV = "&format=csv"

ARG_PERIOD=sys.argv[1]

file_name = "repeat_users-%s.csv" % (ARG_PERIOD.replace(",", "_"))

# Period
PERIOD = "&period=range&date=" + ARG_PERIOD

# this token is used to authenticate your API request.
# You can get the token on the API page inside your Piwik interface i.e.
# By logging in Piwik, then click on your username in the top menu, then click the link “API” in the left menu.
TOKEN_AUTH = "&token_auth=" + settings.TOKEN_AUTH

# dictionary
download_metrics = []

# fn for calling page titles
def get_visits_summary(base_url,module,method,site_id,output_format,period,expanded,api_token):
    API_PAGE_URL = base_url + module + method + site_id + output_format + period + expanded + api_token
    print API_PAGE_URL

    call_visits_summary = requests.get(API_PAGE_URL, verify=False)

    sites = call_visits_summary.json()

    if 'message' in sites:
        print "[FATAL]:  %s. Message: %s" % ("PIWIK not configured properly",
                                            sites_json['message'])
        sys.exit(1)

    data_file = csv.writer(open(file_name,"w"))
    data_file.writerow(['Site ID',
                         'Site Name',
                         'Visits',
                         'Repeat visits'])


    for site_id in sites:
        site = sites[site_id]
        site_name = utils.siteID2name(site_id)
        if (type(site) is dict):
            uniq_visitors = site[u'nb_uniq_visitors']
            visits = site[u'nb_visits']
        else:
            uniq_visitors = 0
            visits = 0
        data_file.writerow([site_id, site_name, visits, visits - uniq_visitors])

    print "Data written out to %s" % file_name





    # # get site id for specific site
    # for site_id in call_visits_summary.json():
    #
    #     if not pages_downloads[site_id]:
    #         continue
    #
    #     API_URL_SITE_NAME = BASE_URL + MODULE + METHOD_SITE_NAME_BY_ID + ID_SITE_NAME + site_id + FORMAT_JSON + TOKEN_AUTH
    #     call_site_name = requests.get(API_URL_SITE_NAME, verify=False)
    #     sites = call_site_name.json()
    #
    #     # get name of specific site
    #     for site_property in sites:
    #         site_name = site_property['name']
    #         # get site property called sub table
    #         for download_property in pages_downloads[site_id]:
    #             # get data from subtable
    #             for downloads in download_property['subtable']:
    #                 downloads_label = downloads['label']
    #                 downloads_hits = downloads['nb_hits']
    #                 downloads_visits = downloads['nb_visits']
    #                 downloads_unique_visits = downloads['sum_daily_nb_uniq_visitors']
    #                 downloads_url = downloads['url']
    #
    #                 # append downloads metrics
    #                 download_metrics.append({"site_id":site_id,
    #                                          "site_name":site_name,
    #                                          "download_label":downloads_label,
    #                                          "download_hits":downloads_hits,
    #                                          "downloads_visits":downloads_visits,
    #                                          "downloads_unique_visits":downloads_unique_visits,
    #                                          "downloads_url":downloads_url})
    #     data_file = csv.writer(open("downloaded-content.csv","w"))
    #     data_file.writerow(['Site ID',
    #                         'Site Name',
    #                         'Download Name',
    #                         'Download Hits',
    #                         'Download Visits','Uniques Visits',
    #                         'Download URL'])
    #
    #     for metric in download_metrics:
    #         data_file.writerow([metric["site_id"],
    #                             metric["site_name"],
    #                             metric["download_label"],
    #                             metric["download_hits"],
    #                             metric["downloads_visits"],
    #                             metric["downloads_unique_visits"],
    #                             metric["downloads_url"]])

get_visits_summary(BASE_URL, MODULE, METHOD_VISITS_SUMMARY, ID_SITES_ALL, FORMAT_JSON,PERIOD,EXPANDED,TOKEN_AUTH)
